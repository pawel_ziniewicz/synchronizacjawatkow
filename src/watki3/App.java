package watki3;

public class App {

	public static void main(String[] args) {
		Wynik wynik = new Wynik(0);
		Dodawanie dodawanie = new Dodawanie(wynik);
		Mnozenie mnozenie = new Mnozenie(wynik);
		Modulo modulo = new Modulo(wynik);
		Odejmowanie odejmowanie = new Odejmowanie(wynik);
		
		//odejmowanie.oblicz();
		
		
		new DodawanieWatek(dodawanie, modulo);
		new MnozenieWatek(mnozenie, dodawanie);
		new OdejmowanieWatek(odejmowanie, mnozenie);
		new ModuloWatek(modulo, odejmowanie);
	}

}
