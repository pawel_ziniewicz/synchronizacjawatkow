package watki3;

public abstract class Obliczenia {

	protected Wynik wynik;
	protected boolean poszlo;
	
	public Obliczenia(Wynik wynik){
		this.wynik=wynik;
		
	}
	
	public abstract void oblicz();

	public Wynik getWynik() {
		return wynik;
	}

	public void setWynik(Wynik wynik) {
		this.wynik = wynik;
	}
	
	public boolean isPoszlo() {
		return poszlo;
	}


	public void setPoszlo(boolean poszlo) {
		this.poszlo = poszlo;
	}
	
}
