package watki3;

public class Wynik {
	
	private double wynik;
	private boolean done;
	private boolean[] tab=new boolean[3];
	


	public Wynik(double wynik){
		this.wynik=wynik;
		for (int i=0; i<tab.length; i++){
			if (i==0) tab[i]=true;
			else tab[i]=false;
		}
		
	}
	
	public boolean getTabI(int i) {
		return tab[i];
	}

	public void setTabI(boolean value, int j) {
		for (int i=0; i<tab.length; i++){
			if (i==j) tab[i]=true;
			else tab[i]=false;
		}
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public double getWynik() {
		return wynik;
	}

	public void dodaj(double a) {
		wynik=wynik+a;
	}
	
	public void pomnoz(double a) {
		wynik=wynik*a;
	}

	public void odejmij(double a) {
		wynik=wynik-a;
	}

	public void modulo() {
		wynik=wynik%2;
	}
	
	

}
